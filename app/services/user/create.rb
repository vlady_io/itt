class User::Create
  def initialize(user_params)
    @params = user_params.dup.to_h
  end

  def call
    validation = User::Validate.new.call(@params)
    user = User.create(@params) if validation.status == 200

    {
      status: validation.status,
      user: user,
      errors: validation.errors.to_h
    }
  end
end