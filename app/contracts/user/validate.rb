class User::Validate < ApplicationContract
  params do
    required(:login).filled(:string)
  end
end