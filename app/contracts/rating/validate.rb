class Rating::Validate < ApplicationContract
  params do
    required(:value).filled(:integer)
    required(:post_id).filled(:integer)
  end

  rule(:value) do
    key.failure('must be in range of 1 to 5') unless value.in?(1..5)
  end
end