class ApplicationContract < Dry::Validation::Contract
  class Dry::Validation::Result
    def status
      success? ? 200 : 422
    end
  end
end