class UsersController < ApplicationController
  def create
    @user = User::Create.new(user_params).call

    render json: @user, status: @user[:status]
  end

  private

  def user_params
    params.require(:user).permit(:login)
  end
end
