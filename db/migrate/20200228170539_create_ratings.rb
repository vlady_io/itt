class CreateRatings < ActiveRecord::Migration[6.0]
  def change
    create_table :ratings do |t|
      t.integer :value, null: false
      t.references :post
    end

    add_index :ratings, :value
  end
end
