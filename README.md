# IT Territory on Rails 🚈

## Models

| Models | | |
|--|--|--|
| User  | Post      | Rating  |
| login | user_id   | post_id |
|       | title     | value   |
|       | content   |         |
|       | author_ip |         |

## Task description

1. Create a post.

   - `title` and `content` (not null)
   - user `login`
     - if there's no User with such `login`, then create new User
   - `author_ip`
   - **return**:
     - post attributes (code 200)
     - validation errors (code 422)

2. Set a post Rating.
    - `post_id` and `value`
    - **returns**: new average rating of the post
    - ⚠️ this action should work correctly with any number of concurrent requests for the same post

3. Get a list of top N posts by their average rating.
    - **returns**: array of `{title:, description:}` of posts

4. Get a list of Author IP's that: `One IP <- different authors`
    - **returns**: array if `{author_ip:, logins: []}`