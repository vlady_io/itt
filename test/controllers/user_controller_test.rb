require 'test_helper'

class UserControllerTest < ActionDispatch::IntegrationTest
  def create_user(user_params)
    post users_url, params: { user: { **user_params } }
  end

  test 'should return status :success for user with login' do
    create_user(login: 'admin')
    assert_equal response_body['status'], 'success'
  end

  test 'should return status :error for user without login' do
    create_user(login: nil)
    assert_equal response_body['status'], 'error'
  end

  test 'should return `must be filled` error message for user without login' do
    create_user(login: nil)
    assert_equal response_body['errors']['login'], ['must be filled']
  end
end
